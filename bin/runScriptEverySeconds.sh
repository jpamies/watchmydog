#!/bin/bash

########### SCRIPT RUN COMMAND EVERY $SECLAUNCH SECONDS #################
#									#
# This scripts will launch function run() every SECLAUNCH seconds. 	#
# If the scripts need more than SECLAUNCH will be launched as soon 	#
# as the last ends.							#
#									#
# 			-- IMPORTANT --					#
#									#
# User needs to add .defaultScripts/runScriptEverySeconds.sh at the 	#
# end and define SECLAUNCH and run funcion.				#
#									#
#########################################################################

##########################
# Seconds between launch #
##########################
# SECLAUNCH=5

#######################
# FUNCTION TO DEFINE  #
#######################

# function run {
# 	date
# 	sleep $(($RANDOM%5+1))
# }



###############################################
#		DO NOT EDIT		      #
###############################################

DATE=`date +%s`

# Launch command

for (( ; ; ))
do
	NOW=`date +%s`
	RES=$(($NOW - $DATE))
	#echo $RES $SECLAUNCH
	if [ $RES -ge $SECLAUNCH ]
	then
		#echo "INSIDE"
		DATE=`date +%s`
		run
	else
		sleep 1
	fi
done
