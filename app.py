from sys import byteorder
from array import array
from struct import pack

import pyaudio
import wave
import time
import os

THRESHOLD = 25500
MIN_THRESHOLD = 3000
CHUNK_SIZE = 1024
FORMAT = pyaudio.paInt16
RATE = 44100
SILENT_TIME = 300
API_KEY = "laW1RZafiRiopBKOqCg9jBzZdmR2O3OYkW2x4dOmBy8jK95U"
FEED_ID = 1113542763

def touch(fname, times=None):
    with file(fname, 'a'):
        os.utime(fname, (times,times))

def is_silent(snd_data):
    "Returns 'True' if below the 'silent' threshold"
    return max(snd_data) < THRESHOLD

def normalize(snd_data):
    "Average the volume out"
    MAXIMUM = 16384
    times = float(MAXIMUM)/max(abs(i) for i in snd_data)

    r = array('h')
    for i in snd_data:
        r.append(int(i*times))
    return r

def add_silence(snd_data, seconds):
    "Add silence to the start and end of 'snd_data' of length 'seconds' (float)"
    r = array('h', [0 for i in xrange(int(seconds*RATE))])
    r.extend(snd_data)
    r.extend([0 for i in xrange(int(seconds*RATE))])
    return r

def save(value):
    try:
    # This will create a new file or **overwrite an existing file**.
    	f = open("/dev/shm/value", "w")
    	try:
    	    f.write(str(value)) # Write a string to a file
    	finally:
    	    f.close()
    except IOError:
    	print("error")
	pass

def record():
    """
    Record a word or words from the microphone and 
    return the data as an array of signed shorts.

    Normalizes the audio, trims silence from the 
    start and end, and pads with 0.5 seconds of 
    blank sound to make sure VLC et al can play 
    it without getting chopped off.
    """
    p = pyaudio.PyAudio()
    stream = p.open(format=FORMAT, channels=1, rate=RATE,
        input=True, output=False,
        frames_per_buffer=CHUNK_SIZE)
    num_silent = 0
    snd_started = False
    latest_time = 0

    r = array('h')

    while 1:
        # little endian, signed short
        snd_data = array('h', stream.read(CHUNK_SIZE))
        if byteorder == 'big':
            snd_data.byteswap()
        silent = is_silent(snd_data)
	
	dttime = time.time() - latest_time
	if dttime > 2:
        	save(max(snd_data))
        	latest_time = time.time()
        if snd_started:
        	print(max(snd_data))
       		r.extend(snd_data)
       		if silent:
       				if num_silent > SILENT_TIME:
       					break
       				else:
       					num_silent +=1
        	else:
        		num_silent = 0

        else:

        	if not silent:
        		snd_started = True

    sample_width = p.get_sample_size(FORMAT)
    stream.stop_stream()
    stream.close()
    p.terminate()
    r = add_silence(r, 0.5)
    return sample_width, r

def record_to_file(path):
    "Records from the microphone and outputs the resulting data to 'path'"
    filename = time.strftime("%Y%m%d-%H%M%S")
    sample_width, data = record()
    data = pack('<' + ('h'*len(data)), *data)
    print("save to file "+filename)
    wf = wave.open(path+filename+".wav", 'wb')
    wf.setnchannels(1)
    wf.setsampwidth(sample_width)
    wf.setframerate(RATE)
    wf.writeframes(data)
    wf.close()
    print("saved ")
    touch("filename.wav.done", time.time())

if __name__ == '__main__':
    print("waiting for sound")

    record_to_file('/dev/shm/')

    print("done")
