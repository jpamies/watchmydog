#!/usr/bin/expect
set timeout 2
set message [lindex $argv 1]
set destination [lindex $argv 0]
spawn /opt/tg/telegram
expect "> "
sleep 1
send "\rmsg $destination $message\r"
expect "> "
expect eof
